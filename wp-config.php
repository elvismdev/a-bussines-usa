<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'abusinessusa');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B7A(h7vA-R _zbqr37;62h6Dvf&,lr`pFG,i_8~FKm-a1/F% QL:Xj[$ja<+|[S|');
define('SECURE_AUTH_KEY',  '8I[At&Fbu*([}d!r*lRHZKVo+*+DMucz#{#V!f:.0;_<$Kg<SY7j)]+[7cr|`yw5');
define('LOGGED_IN_KEY',    'dafGP3E[7e85#iwk4yqBg{ ;N?u]WGB{GfP39GrM?pOY((1*<`)L@[IDK.%xn%?Z');
define('NONCE_KEY',        ',* +`vW^^wLj+pQC@4+s-B.)Pj>T1_yKR=I<||behmx;$d>+DvJLAxiH^?xd?5h_');
define('AUTH_SALT',        'S&x<h,X!.G/r=LWS;Ed@QsiZp;}j+fe=sMS{2,hicN?Gl-~%W%iUA%KfM5V7fFZx');
define('SECURE_AUTH_SALT', '+(#@ta1uN17-)y=?M@I{)>0P?+V(L&+/Dw*HDH(|$k/xW!uc/-9-8dK?jZ)#iE{&');
define('LOGGED_IN_SALT',   'sdBwD;P/_Zm-?If,V~)<w,XIXs@~n))?[o)q0&`p`m#pHlT^HNjwiZopE$LL,I3L');
define('NONCE_SALT',       'h2JtVnn{VBWxRAY)l+Bh[#L$P:NP[`DB8 c1q|qz7X!StD/0w<aDrv^SlY_Ttg%e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'abusa_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');
